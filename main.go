// Copyright 2024 Daniel Erat.
// All rights reserved.

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"slices"
	"strconv"
	"strings"
	"time"
)

const (
	moviesFilename = "movies.json"

	textWidth       = 80
	movieListHeight = 10
	imageWidthSmall = 40
	imageWidthBig   = 80

	clear     = "\033[2J"
	home      = "\033[H"
	reset     = "\033[0m"
	bold      = "\033[1m"
	italic    = "\033[3m"
	underline = "\033[4m"
)

func main() {
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage: %v [flag]...>\n"+
			"Displays Daily Tomato movie trivia.\n\n", os.Args[0])
		flag.PrintDefaults()
	}
	date := flag.String("date", time.Now().Format(time.DateOnly),
		"Day to display as YYYY-MM-DD")
	dir := flag.String("dir", filepath.Join(os.Getenv("HOME"), "temp/daily_tomato"),
		"Directory containing JSON files")
	filter := flag.Bool("filter-movies", true, "Filter movies based on correct words")
	flag.Parse()
	if flag.NArg() != 0 {
		flag.Usage()
		os.Exit(2)
	}

	for exe, pkg := range map[string]string{
		"fzf":    "fzf",
		"catimg": "catimg",
	} {
		if _, err := exec.LookPath(exe); err != nil {
			fmt.Fprintf(os.Stderr, "%v not found; try \"apt-get install %v\"", exe, pkg)
			os.Exit(1)
		}
	}

	movies, err := readMovieList(filepath.Join(*dir, moviesFilename))
	if err != nil {
		fmt.Fprintln(os.Stderr, "Failed reading movie list:", err)
		os.Exit(1)
	}

	game, challenge, err := readGame(filepath.Join(*dir, *date+".json"))
	if err != nil {
		fmt.Fprintln(os.Stderr, "Failed reading game:", err)
		os.Exit(1)
	}
	titleWords := make(map[string]struct{})
	for _, w := range strings.Fields(game.Title) {
		titleWords[strings.ToLower(w)] = struct{}{}
	}

	// Drop all titles that don't have the correct number of words.
	if *filter {
		cnt := len(strings.Fields(game.Title))
		var filtered []string
		for _, m := range movies {
			if len(strings.Fields(m)) == cnt {
				filtered = append(filtered, m)
			}
		}
		movies = filtered
	}

	// Print the heading.
	fmt.Print(clear + home)
	fmt.Println(bold + game.ReleaseDate.Format(time.DateOnly) + reset)
	if challenge.Name != "" {
		fmt.Printf("Challenge: %v (%v - %v)\n", italic+challenge.Name+reset,
			challenge.StartDate.Format(time.DateOnly), challenge.EndDate.Format(time.DateOnly))
	}

	guess := 1
	maxGuesses := 1 + len(game.Reviews) + 1
Loop:
	for ; true; guess++ {
		switch {
		case guess == 1:
			var wordsStr string
			if n := len(strings.Fields(game.Title)); n == 1 {
				wordsStr = "1 word"
			} else {
				wordsStr = fmt.Sprintf("%d words", n)
			}
			fmt.Println()
			fmt.Printf("%d. %v: %d%% critics, %d%% audiences\n",
				guess, bold+wordsStr+reset, game.Tomatometer, game.AudienceScore)
			fmt.Printf("   %v\n\n", game.Metadata)
		case guess < maxGuesses:
			review := game.Reviews[guess-2]
			desc := review.Desc
			for from, to := range map[string]string{
				"<b>":   bold,
				"</b>":  reset,
				"<em>":  italic,
				"</em>": reset,
			} {
				desc = strings.ReplaceAll(desc, from, to)
			}
			lns := wrap(desc, textWidth-3)
			for i, ln := range lns {
				if i == 0 {
					fmt.Printf("%d. “%v\n", guess, ln)
				} else if i < len(lns)-1 {
					fmt.Println("   " + ln)
				} else {
					fmt.Println("   " + ln + reset + "”")
				}
			}
			fmt.Printf("   —%v\n\n", review.CriticName)
		case guess == maxGuesses:
			fmt.Printf("%d. Poster:\n", guess)
			if err := displayImage(game.PosterURL, imageWidthSmall); err != nil {
				fmt.Fprintln(os.Stderr, "Failed displaying poster:", err)
			}
		default:
			break Loop
		}

		movie, err := promptForMovie(movies)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Failed getting guess:", err)
			os.Exit(1)
		}
		if movie == strings.TrimSpace(game.Title) {
			break
		}

		words := strings.Fields(movie)
		var knownWords []string
		for i, w := range words {
			if _, ok := titleWords[strings.ToLower(w)]; ok {
				words[i] = underline + w + reset
				knownWords = append(knownWords, w)
			}
		}
		fmt.Println("> " + strings.Join(words, " "))
		fmt.Println()

		if *filter && len(knownWords) > 0 {
			movies = filterMovies(movies, knownWords)
		}
	}

	fmt.Println(bold + game.Title + reset)
	for _, ln := range wrap(game.Synopsis, textWidth) {
		fmt.Println(ln)
	}
	if guess < maxGuesses {
		fmt.Println()
		displayImage(game.PosterURL, imageWidthBig)
	}
	fmt.Println()
}

// game describes a given day's game.
type game struct {
	Title    string `json:"title"`
	Synopsis string `json:"synopsis"`

	Metadata        string `json:"metadata"` // e.g. "PG-13, 1996, Drama, 2h8m"
	CriticConsensus string `json:"criticConsensus"`

	Tomatometer   int `json:"tomatometer"`   // [0, 100]
	AudienceScore int `json:"audienceScore"` // [0, 100]

	MovieURL  string `json:"movieUrl"`
	PosterURL string `json:"posterUrl"`

	ReleaseDate time.Time `json:"releaseDate"` // of game, not of movie

	Reviews []struct {
		IsFresh    bool   `json:"isFresh"`
		Desc       string `json:"desc"`
		CriticName string `json:"criticName"`
		ReviewURL  string `json:"reviewUrl"`
	} `json:"reviews"`
}

// challenge describes a themed multi-day challenge.
type challenge struct {
	Name      string    `json:"name"`
	StartDate time.Time `json:"startDate"`
	EndDate   time.Time `json:"endDate"`
}

// readGame reads a JSON file at p containing a given day's game and optional challenge.
func readGame(p string) (game, challenge, error) {
	f, err := os.Open(p)
	if err != nil {
		return game{}, challenge{}, err
	}
	defer f.Close()

	var data struct {
		Data struct {
			Game      game      `json:"game"`
			Challenge challenge `json:"challenge"`
		} `json:"data"`
	}
	if err := json.NewDecoder(f).Decode(&data); err != nil {
		return game{}, challenge{}, err
	}
	return data.Data.Game, data.Data.Challenge, nil
}

// readMovieList reads a JSON file at p containing the list of all movies.
func readMovieList(p string) ([]string, error) {
	f, err := os.Open(p)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var data struct {
		Data []string `json:"data"`
	}
	if err := json.NewDecoder(f).Decode(&data); err != nil {
		return nil, err
	}

	// Remove duplicates.
	movies := data.Data
	slices.Sort(movies)
	movies = slices.Compact(movies)
	return movies, nil
}

// filterMovies returns a new slice containing only movies in the supplied list that
// contain all of the supplied words.
func filterMovies(movies, knownWords []string) []string {
	var res []*regexp.Regexp
	for _, w := range knownWords {
		res = append(res, regexp.MustCompile(`(?i)\b`+regexp.QuoteMeta(w)+`\b`))
	}

	var filtered []string
Loop:
	for _, m := range movies {
		for _, re := range res {
			if !re.MatchString(m) {
				continue Loop
			}
		}
		filtered = append(filtered, m)
	}
	return filtered
}

// promptForMovie runs fzf to prompt for a movie from the supplied list.
func promptForMovie(movies []string) (string, error) {
	cmd := exec.Command(
		"fzf",
		"-i", // case-insensitive match
		"--exact",
		"--height="+strconv.Itoa(movieListHeight),
		"--layout=reverse",
	)
	cmd.Stdin = strings.NewReader(strings.Join(movies, "\n"))
	cmd.Stderr = os.Stderr
	out, err := cmd.Output()
	return strings.TrimSpace(string(out)), err
}

// displayImage downloads the image at url and runs catimg to display it
// at the supplied width.
func displayImage(url string, width int) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return fmt.Errorf("%v: %v", resp.StatusCode, resp.Status)
	}

	f, err := os.CreateTemp("", "taily_domato.*.jpg")
	if err != nil {
		return err
	}
	defer os.Remove(f.Name())

	if _, err := io.Copy(f, resp.Body); err != nil {
		f.Close()
		return err
	}
	cmd := exec.Command("catimg", "-w", strconv.Itoa(width), f.Name())
	cmd.Stdout = os.Stdout
	return cmd.Run()
}

// wrap splits orig into one or more lines of the supplied maximum length.
// Whitespace is not preserved; words are separated by single spaces.
func wrap(orig string, max int) []string {
	lines := []string{""}
	for _, w := range strings.Fields(orig) {
		li := len(lines) - 1
		if lines[li] == "" {
			lines[li] = w
		} else if len(lines[li])+1+len(w) <= max {
			lines[li] += " " + w
		} else {
			lines = append(lines, w)
		}
	}
	return lines
}
