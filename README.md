# taily-domato

This is an alternate CLI frontend to the [Rotten Tomatoes Daily Tomato] movie
trivia game. It is unaffiliated with Rotten Tomatoes.

You'll need to provide your own `movies.json` and `YYYY-MM-DD.json` files.

[Rotten Tomatoes Daily Tomato]: https://www.rottentomatoes.com/movie-trivia/

## Usage

```
Usage: taily-domato [flag]...>
Displays Daily Tomato movie trivia.

  -date string
    	Day to display as YYYY-MM-DD (default "2024-05-08")
  -dir string
    	Directory containing JSON files (default "$HOME/temp/daily_tomato")
  -filter-movies
    	Filter movies based on correct words (default true)
```
